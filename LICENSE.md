# License

All karaokes are property of their respective authors.

Videos stored on Shelter are the property of their respective right holders.

We try, as much as possible, to fill the `author` tag in a `.kara.json` file to credit the author of a karaoke. Some of them are under the "Karaoke Mugen" name simply because we are not sure of who created them. If you know, please submit a pull request or contact us.

The files in this repository are under a [Creative Commons 4.0 BY-NC-SA license](http://creativecommons.org/licenses/by-nc-sa/4.0/)

If you have questions about the license or wish to ask for a specific permission, feel free to [contact us](http://mugen.karaokes.moe/en/contact.html) or [open an issue](https://gitlab.com/karaokemugen/bases/karaokebase-world/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
